package keti.lib.smil;

/**
 * Smil Tag 정보를 저장하기 위한 클래스
 * @author eajang
 * @version 1.0
 * @since 2014.12.03
 */
public class SmilDataInfo {

	private String sTagName = null;
	private String sText = null;
	private String sAttrName = null;
	private String sAttrValue = null;
	
	public static String TAG_SMIL = "smil";
	public static String TAG_HEAD = "head";
	public static String TAG_BODY = "body";
	public static String TAG_PAR = "par";
	public static String TAG_SEQ = "seq";
	public static String TAG_AUDIO = "audio";
	public static String TAG_TEXT = "text";
		
	public String getTagName() {
		return sTagName;
	}

	public void setTagName(String sTagName) {
		this.sTagName = sTagName;
	}

	public String getText() {
		return sText;
	}

	public void setText(String sText) {
		this.sText = sText;
	}

	public String getAttrName() {
		return sAttrName;
	}

	public void setAttrName(String sAttrName) {
		this.sAttrName = sAttrName;
	}

	public String getAttrValue() {
		return sAttrValue;
	}

	public void setAttrValue(String sAttrValue) {
		this.sAttrValue = sAttrValue;
	}

}
