package keti.lib.smil;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

/**
 * SMIL Parser Class
 * @author eajang
 * @version 1.0
 * @since 2014.12.03
 */
public class SmilParser {

	
	private XmlPullParserFactory oPPFactory = null;
	private XmlPullParser oPParser = null;
	
	private URL oUrl = null;
	private ArrayList<SmilDataInfo> aDataList = null;
	
	public SmilParser() {		

		try {
			oPPFactory = XmlPullParserFactory.newInstance();
			oPPFactory.setNamespaceAware(true);
			oPParser = oPPFactory.newPullParser();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public ArrayList<SmilDataInfo> parseUrl(String sUrl) {
		
		try {
			oUrl = new URL(sUrl);
			oPParser.setInput(oUrl.openStream(), "UTF-8");
			aDataList = new ArrayList<SmilDataInfo>();
			parseData(oPParser, aDataList);			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return aDataList;
	}
	
	public ArrayList<SmilDataInfo> parseXmlFile(InputStream oInputStream) {

			try {
				oPParser.setInput(oInputStream, "UTF-8");
				oPParser.next();
				aDataList = new ArrayList<SmilDataInfo>();
				parseData(oPParser, aDataList);
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return aDataList;
		
	}
	
	public void parseData(XmlPullParser oPParser, ArrayList<SmilDataInfo> aDataList) {
		
		try {
			int iEventType = oPParser.getEventType();
			boolean bIsItemTag = false;
			SmilDataInfo aData = null;
			String sTagName = "";
			while (iEventType != XmlPullParser.END_DOCUMENT) {
				if (iEventType == XmlPullParser.START_TAG) {
					sTagName = oPParser.getName();
					aData = new SmilDataInfo();
					aData.setTagName(sTagName);
					bIsItemTag = true;
				} else if (iEventType == XmlPullParser.TEXT && bIsItemTag) {
					if (sTagName.equalsIgnoreCase(SmilDataInfo.TAG_TEXT) || sTagName.equalsIgnoreCase(SmilDataInfo.TAG_AUDIO)) {
						aData.setText(oPParser.getText());
						aDataList.add(aData);
						aData = null;
					}
				} else if (iEventType == XmlPullParser.END_TAG) {
					sTagName = oPParser.getName();
					bIsItemTag = false;
				}
				iEventType = oPParser.next();
			}
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
